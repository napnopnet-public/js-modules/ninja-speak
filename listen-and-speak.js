export { ListenAndSpeak }

class ListenAndSpeak {
    constructor(){
        navigator.getUserMedia = ( navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);

        this.audioContext;
        this.mediaStream;
        this.mediaRecorder;
        this.activeBlocks = [];

        // see https://www.html5rocks.com/en/tutorials/webaudio/intro/
        let thisHere = this; //needed for scope in init function
        window.addEventListener('load', init, false);
        function init() {
            try {
                // Fix up for prefixing
                window.AudioContext = window.AudioContext||window.webkitAudioContext;
            }
            catch(e) { //werkt niet meer?
                console.log("Web Audio API is not supported in this browser");
                alert('Web Audio API is not supported in this browser');
            }
        }

        this.thisTimeOut;
    }

    /**
     * Stops the current sound recording.
     * This function can be called by clicking the appropriate stop button, of by the timeout of the recording.
     *
     * @param {object} target - DOM element - Can be a record button whose action is stopped by a timeout, or a stop button
     */
    stopRecording(target){
        //re-enable record buttons
        this.activeBlocks.forEach(function(value){
            let recordButtons = value.querySelectorAll("button.record");
            // stopped,so enable all record buttons
            recordButtons.forEach(function(element){
                element.removeAttribute("disabled");
            });
        });

        let thisRecordButton;
        if (target.tagName === "BUTTON"){
            if (target.classList.contains("record")){
                thisRecordButton = target;
            }else if(target.classList.contains("stop")){
                clearTimeout(this.thisTimeOut);
                thisRecordButton = target.previousElementSibling.querySelector("button.record");
            }else{
                console.log("unkown error");
            }
        }
        thisRecordButton.style.background = "";
        thisRecordButton.style.color = "";
        this.mediaRecorder.stop();
    }

    /**
     * Prepare and start recording sound.
     *
     * @param {object} target - DOM element - the record button clicked
     */
    startRecording (target) {
        let thisHere = this; //needed for callbacks here

        // console.log(target.parentElement.nextElementSibling);

        this.activeBlocks.forEach(function(value){
            let recordButtons = value.querySelectorAll("button.record");
            // while recording, disable all record buttons
            recordButtons.forEach(function(element){
                element.setAttribute("disabled","disabled");
            });
        });

        target.style.background = "red";
        target.style.color = "black";

        //start recording
        this.mediaRecorder.start();

        // set a timeout to stop recording
        this.thisTimeOut = setTimeout(function () {
            thisHere.stopRecording(target);
        },3000);

        // ondataavailable is fired when recording is stopped
        this.mediaRecorder.ondataavailable = function(e){

            let downloadAnker = target.parentElement.nextElementSibling.nextElementSibling;


            let thisLi = target.parentElement.parentElement.parentElement;
            let pathstring = thisLi.querySelector("span[data-soundid]").getAttribute("data-soundid");

            // cleanup string
            let searchreg = /\.\/sounds\//g;
            let filestring = pathstring.replace(searchreg, '');

            let searchreg2 = /\.ogg/g;
            let filetitle = filestring.replace(searchreg2, '');

            let clipName = "jouw " + filetitle;
            let clipContainer = document.createElement('article');
            let clipLabel = document.createElement('p');
            let audio = document.createElement('audio');

            clipContainer.classList.add('clip');
            audio.setAttribute('controls', '');
            clipLabel.innerHTML = clipName;
            clipContainer.appendChild(clipLabel);
            clipContainer.appendChild(audio);

            let thisSoundClip = target.parentElement.parentElement.querySelector("article.sound-clip");
            thisSoundClip.innerHTML = "";
            thisSoundClip.appendChild(clipContainer);

            let datatype = e.data.type;
            let subdatatypestring = datatype.replace(/^audio\//,'');
            let extension = subdatatypestring.replace( /;.*$/,"");

            downloadAnker.download = Date.now() + "_mijn_" + filetitle + "." + extension;
            let blobURL = URL.createObjectURL(e.data);
            audio.src = blobURL;
            downloadAnker.href = blobURL;
            downloadAnker.type = e.data.type;
        }
    }

    /**
     *
     * @param {object} target - DOM element - The record button clicked
     */
    createMediarecorder(target){
        let thisHere = this; //needed for callbacks here
        if (navigator.getUserMedia) {
            // console.log('getUserMedia supported.');
            if (this.mediaStream === undefined){
                navigator.getUserMedia (
                    // constraints - only audio needed for this app
                    {
                        audio: true
                    },
                    // Success callback
                    function(stream) {
                        thisHere.mediaStream = stream;

                        // now create the recorder
                        thisHere.mediaRecorder = new MediaRecorder(thisHere.mediaStream);
                        thisHere.startRecording(target);
                    },

                    // Error callback
                    function(err) {
                        console.log('The following gUM error occured: ' + err);
                    }
                );
            }
        }else {
            console.log('getUserMedia not supported on your browser!');
            alert("sorry, dit gaat niet werken in uw browser");
        }
    }

    /**
     *
     * @param {object} thisSpeaker - DOM element - The current speaker icon clicked on
     * @param {string} oldColor - The color of the speaker icon before the click
     * @param {object} audio - decoded audio - The audio data to play
     */
    playSound(thisSpeaker, oldColor, audio) {

        const playSound = this.audioContext.createBufferSource();
        playSound.buffer = audio;
        playSound.connect(this.audioContext.destination);
        playSound.start(this.audioContext.currentTime);
        playSound.onended = function(){
            thisSpeaker.style.color = oldColor;
        }
    }

    /**
     *
     * @param {string} soundFilePath
     * @param {object} thisSpeaker - DOM element
     * @param {string} oldColor
     */
    getSoundFromFile(soundFilePath, thisSpeaker, oldColor){

        let thisHere = this; //needed for callbacks here

        if(this.audioContext === undefined){
            this.audioContext = new AudioContext();
        }

        fetch(soundFilePath)
        .then(data => data.arrayBuffer())
        .then(arrayBuffer => this.audioContext.decodeAudioData(arrayBuffer))
        .then(decodedAudio => {
                // audio = decodedAudio;
                thisHere.playSound(thisSpeaker,oldColor,decodedAudio);
            }
        );
    }

    /**
     *
     * @param {object} targetDOMpart - DOM element - The DOM element receiving initialisation
     */
    initiateMultipleChoice(targetDOMpart){

        let thisHere = this; //needed for callbacks here
        this.activeBlocks.push(targetDOMpart);

        targetDOMpart.addEventListener("click", function(event){

                if(event.target.classList.contains("speaker")){
                    let oldColor = event.target.style.color;
                    let thisSpeaker = event.target;
                    event.target.style.color = "red";

                    let soundFilePath = event.target.attributes.getNamedItem("data-soundid").value;
                    thisHere.getSoundFromFile(soundFilePath,thisSpeaker,oldColor);
                }
                if (event.target.classList.contains("record")) {
                    if(thisHere.mediaRecorder === undefined){
                        thisHere.createMediarecorder(event.target);
                    }else{
                        thisHere.startRecording(event.target);
                    }
                }
                if(event.target.classList.contains("stop")){
                    thisHere.stopRecording(event.target);
                }
            }
        );
    }
}
